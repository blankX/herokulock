import asyncio
import logging
import yaml
from telethon import TelegramClient, events

logging.basicConfig(level=logging.INFO)
with open('config.yaml') as file:
    config = yaml.safe_load(file.read())
api_id = config['telegram']['api_id']
api_hash = config['telegram']['api_hash']
bot_token = config['telegram'].get('bot_token')
lock_chat_id = config['config']['lock_chat_id']
lock_wait_delay = config['config']['lock_wait_delay']
command_when_unlocked = config['config']['command_when_unlocked']

client = TelegramClient(None, api_id, api_hash)
@client.on(events.NewMessage(lock_chat_id, outgoing=True, pattern='^herokulock check$'))
async def herokulock_check(e):
    await e.reply('herokulock online')

@client.on(events.NewMessage(lock_chat_id, outgoing=True, pattern='^herokulock online$'))
async def herokulock_online(e):
    logging.error('A herokulock instance is running, exiting')
    exit(1)

async def main():
    await client.start(bot_token=bot_token)
    await client.send_message(lock_chat_id, 'herokulock check')
    await asyncio.sleep(lock_wait_delay)
    proc = await asyncio.create_subprocess_shell(command_when_unlocked)
    await proc.communicate()
    exit(proc.returncode)

client.loop.run_until_complete(main())
